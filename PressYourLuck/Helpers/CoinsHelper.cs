﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PressYourLuck.Helpers
{
    public static class CoinsHelper
    {
        /*
         * Consider using this helper to Get and Set the Current Bet and the original bet
         * (both in session variables), as well as adding a Get and Set for the player's
         * total number of coins (which we'll store in Cookies)
         * 
         * HINT: Remember that HttpContext as well as Response and Request objects are not
         * available from here, so you may need to pass those in from your controller.
         * 
         * I've coded the first one for you and have created placeholders for the rest.
         * 
         */
        //save session for current bet
        public static void SaveCurrentBet(HttpContext httpContext, double bet)
        {
            httpContext.Session.SetString("current-bet", bet.ToString("N2"));
        }

        //get session for current bet
        public static double GetCurrentBet(HttpContext httpContext)
        {       
            return Convert.ToDouble(httpContext.Session.GetString("current-bet"));
        }
        //save session for original bet
        public static void SaveOriginalBet(HttpContext httpContext, double originalBet)
        {
            httpContext.Session.SetString("original-bet", originalBet.ToString("N2"));
        }
        //get session for current bet
        public static double GetOriginalBet(HttpContext httpContext)
        {
            return Convert.ToDouble(httpContext.Session.GetString("original-bet"));
        }
        //save cookies for total coin
        public static void SaveTotalCoins(HttpResponse httpResponse, double bet)
        {
            httpResponse.Cookies.Append("total-coins", bet.ToString());
        }
        //get cookies for total coin
        public static double GetTotalCoins(HttpRequest httpRequest)
        {
            return Convert.ToDouble(httpRequest.Cookies["total-coins"]);

        }


    }
}
