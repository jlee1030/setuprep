﻿using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using PressYourLuck.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PressYourLuck.Helpers
{
    public static class GameHelper
    {
        //This creates a collection of 12 tiles with randomly generated values
        public static List<Tile> GenerateNewGame()
        {
            var tileList = new List<Tile>();
            Random r = new Random();
            for (int i = 0; i < 12; i++)
            {
                double randomValue = 0;
                if (r.Next(1, 4) != 1)
                {
                    randomValue = (r.NextDouble() + 0.5) * 2;
                }

                var tile = new Tile()
                {
                    TileIndex = i,
                    Visible = false,
                    Value = randomValue.ToString("N2")
                };

                tileList.Add(tile);
            }
            return tileList;
        }

        /*
        * There are MANY other helpers you may want to create here.  I've created some
        *  placeholder as well as hints for others you may find useful:
        *
        * 
        * HINT: Remember that your HttpContext is not available in this class so you may
        * need to pass it into methods that need it!
        * 
        */


        public static List<Tile> GetCurrentGame(HttpContext httpContext)
        {
            var tileList = new List<Tile>();

            var tileJSON = httpContext.Session.GetString("tileList");

            if (string.IsNullOrEmpty(httpContext.Session.GetString("tileList")))
            {
                tileList = GenerateNewGame();
                SaveCurrentGame(httpContext, tileList);
            }
            else
            {
                tileList = DeserializeTileList(tileJSON);
            }

            return tileList;
        }
        // save current game session
        public static void SaveCurrentGame(HttpContext httpContext, List<Models.Tile> tileList)
        {
            httpContext.Session.SetString("tileList", JsonConvert.SerializeObject(tileList));
        }
        
        public static double PickTileAndUpdateGame(HttpContext httpContext, List<Models.Tile> tileList, int id)
        {
            GameHelper.GetCurrentGame(httpContext);
            double crtBet = 0;
            //if user pick bust, display all
            //update current bet price and current game 
            if (tileList[id].Value == "0" || tileList[id].Value == "0.00")
            {
                for (int i = 0; i < 12; i++)
                {
                    tileList[i].Visible = true;
                    GameHelper.SaveCurrentGame(httpContext, tileList);
                    CoinsHelper.SaveCurrentBet(httpContext, crtBet);
                }
                
            }
            else
            {
                //user not bust update current bet and current game
                crtBet=Convert.ToDouble(tileList[id].Value) * Convert.ToDouble(CoinsHelper.GetCurrentBet(httpContext));
                GameHelper.SaveCurrentGame(httpContext, tileList);
                CoinsHelper.SaveCurrentBet(httpContext, crtBet);
            }
            return crtBet;
        }
        //remove current session game
        public static void ClearCurrentGame(HttpContext httpContext, List<Models.Tile> tileList)
        {
            httpContext.Session.Remove("tileList");
        }

        public static List<Tile> DeserializeTileList(string json)
        {
            var results = new List<Tile>();
            results = JsonConvert.DeserializeObject<List<Models.Tile>>(json);
            return results;
        }

    }
}
