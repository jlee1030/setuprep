﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using PressYourLuck.Helpers;
using PressYourLuck.Models;
using PressYourLuck.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PressYourLuck.Controllers
{
    public class GameController : Controller
    {
        public IActionResult Index()
        {
            //get corrent game
            var tileList = GameHelper.GetCurrentGame(HttpContext);

            GameViewModel gvm = new GameViewModel();
            {
                //assign current game to game view model
                gvm.TileList = tileList;
                gvm.CurrentBet = CoinsHelper.GetCurrentBet(HttpContext);
            }

            return View(gvm);
        }
        //get the id from index page
        [HttpGet("/Game/Index/{id}")]
        public IActionResult revealCard(int id)
        {
            //id =tile index 
            //get current game and visible index of tile list 
            var tileList = GameHelper.GetCurrentGame(HttpContext);
            tileList[id].Visible = true;
            //update tile and current game
            GameHelper.PickTileAndUpdateGame(HttpContext, tileList, id);
            GameHelper.SaveCurrentGame(HttpContext, tileList);

            return RedirectToAction("Index", "Game");
        }
        public IActionResult newGame()
        {
            //save player name and amount date and audit type to session then send it to database
            var audit = new Audit();
            audit.PlayerName = HttpContext.Request.Cookies["Player-name"];
            audit.Amount = CoinsHelper.GetOriginalBet(HttpContext);
            audit.CreatedDate = DateTime.Now;
            //type is lose
            audit.AuditTypeId = "D";
            HttpContext.Session.SetString("auditLose", JsonConvert.SerializeObject(audit));
            //clear the current game session
            var tileList = new List<Tile>();
            GameHelper.ClearCurrentGame(HttpContext, tileList);
            return RedirectToAction("Index", "Audit");
            
        }
        public IActionResult endGame()
        {
            // //save player name and amount date and audit type to session then send it to database
            var audit = new Audit();
            audit.PlayerName = HttpContext.Request.Cookies["Player-name"];
            audit.Amount = CoinsHelper.GetCurrentBet(HttpContext);
            audit.CreatedDate = DateTime.Now;
            //type is win
            audit.AuditTypeId = "C";
            HttpContext.Session.SetString("auditWin", JsonConvert.SerializeObject(audit));
            //temp message for jackpot with price
            TempData["message"] = $"BIG WINNER!You chased out for {CoinsHelper.GetCurrentBet(HttpContext)} coins!Care to press your luck again";
            // get the totalcoin and current
            var tileList = new List<Tile>();
            double totalCoin = CoinsHelper.GetTotalCoins(Request);
            double currentBet = CoinsHelper.GetCurrentBet(HttpContext);
            //clear current game session
            GameHelper.ClearCurrentGame(HttpContext, tileList);
            //update total coin
            CoinsHelper.SaveTotalCoins(Response, totalCoin + currentBet);

            return RedirectToAction("Index", "Audit");
        }


    }
}
