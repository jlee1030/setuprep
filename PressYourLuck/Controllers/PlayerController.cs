﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using PressYourLuck.Helpers;
using PressYourLuck.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace PressYourLuck.Controllers
{
    public class PlayerController : Controller
    {
        [HttpGet]
        public IActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public IActionResult Index(Player player)
        {
            //if form is valid save the cookies for name and coin, and save it to session,then send it to database 
            if(ModelState.IsValid)
            {
                HttpContext.Response.Cookies.Append("Player-name", player.Name);
                HttpContext.Response.Cookies.Append("total-coins", player.TotalCoins.ToString());
                var audit = new Audit();
                audit.PlayerName = player.Name;
                audit.Amount = player.TotalCoins;
                audit.CreatedDate = DateTime.Now;
                audit.AuditTypeId = "A";
                HttpContext.Session.SetString("auditCashIn", JsonConvert.SerializeObject(audit));
                return RedirectToAction("Index", "Audit");
            }
            else
            {
                return View(player);
            }
        }
        public IActionResult CashOut()
        {
            //if cookie for player is not empty save the assign the cookie to audit list and save it to session then delete
            //cookie for name and total coin then send it to database. then display temp message for totalcoin
            if (Request.Cookies["Player-name"] != null)
            {
                var audit = new Audit();
                audit.PlayerName = HttpContext.Request.Cookies["player-name"];
                audit.Amount = CoinsHelper.GetTotalCoins(Request);
                audit.CreatedDate = DateTime.Now;
                audit.AuditTypeId = "B";
                HttpContext.Session.SetString("auditCashOut", JsonConvert.SerializeObject(audit));
                Response.Cookies.Delete("Player-name");
                Response.Cookies.Delete("total-coins");
            }
            TempData["message"] = $"You cashed out for {CoinsHelper.GetTotalCoins(Request)} coins";
            return RedirectToAction("Index", "Audit");
        }
    }
}
