﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using PressYourLuck.Helpers;
using PressYourLuck.Models;
using PressYourLuck.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace PressYourLuck.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;

        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }
        [HttpGet]
        public IActionResult Index()
        {
            //if player name is empty return to player index page
            if (string.IsNullOrEmpty(HttpContext.Request.Cookies["player-name"]))
            {
                return RedirectToAction("Index", "Player");
            }
            //save the cookie for name
            HomeViewModel homeViewModel = new HomeViewModel()
            {
                PlayerName = HttpContext.Request.Cookies["player-name"],
                bet = 0
            };

            return View(homeViewModel);
        }

        [HttpPost]
        public IActionResult Index(HomeViewModel hvm)
        {
            //if player bet is valid
            if (ModelState.IsValid)
            {
                //assign total coin in totalcoin variable
                var totalCoin = CoinsHelper.GetTotalCoins(Request);
                //id bet>totalcoin, display error
                if(hvm.bet>totalCoin)
                {
                    TempData["message"] = "You cannot bet more than you have!";
                    //if total coin is 0, error message
                    if (totalCoin == 0)
                    {
                        TempData["message"] = "You’ve lost all your coins and must enter more to keep playing";
                    }
                    return RedirectToAction("Index","Home");
                }
               //update totalcoin, original , currentbet
                CoinsHelper.SaveTotalCoins(Response, totalCoin - hvm.bet);
                CoinsHelper.SaveOriginalBet(HttpContext, hvm.bet);
                CoinsHelper.SaveCurrentBet(HttpContext, hvm.bet);
                return RedirectToAction("Index", "Game");
            }
            //form is invalid
            else
            {
                return RedirectToAction("Index","Home");
            }
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
