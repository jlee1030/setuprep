﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using PressYourLuck.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PressYourLuck.Controllers
{
    public class AuditController : Controller
    {
        private readonly PressYourLuckContext _context;

        public AuditController(PressYourLuckContext context)
        {
            _context = context;
        }

        // GET: Audits
        //get id from index
        public IActionResult Index(int? id)
        {
            //if there's no audit sesstion
            if(HttpContext.Session.GetString("Audit")==null)
            {
                //default audit all
                HttpContext.Session.SetString("Audit", "All");
                
            }
            //if id=1 save session for all
            if (id == 1)
            {
                HttpContext.Session.SetString("Audit", "All");
            }
            //if id=1 save session for cash in
            else if (id == 2)
            {
                HttpContext.Session.SetString("Audit", "CashIn");
            }
            //if id=1 save session for cash out
            else if (id == 3)
            {
                HttpContext.Session.SetString("Audit", "CashOut");
            }
            //if id=1 save session for lose
            else if (id == 4)
            {
                HttpContext.Session.SetString("Audit", "Lose");
            }
            //if id=1 save session for win
            else if (id == 5)
            {
                HttpContext.Session.SetString("Audit", "Win");
            }

            //if audit session is all, id =1
            if (HttpContext.Session.GetString("Audit") == "All")
            {
                id = 1;
                //tempdata id= 1 this is to remember session in the audit page
                TempData["id"] = "1";
            }
            //if audit session is all, id =1
            else if (HttpContext.Session.GetString("Audit") == "CashIn")
            {
                id = 2;
                TempData["id"] = "2";
            }
            //if audit session is cash out, id =2
            else if (HttpContext.Session.GetString("Audit") == "CashOut")
            {
                id = 3;
                TempData["id"] = "3";
            }
            //if audit session is lose, id =3
            else if (HttpContext.Session.GetString("Audit") == "Lose")
            {
                id = 4;
                TempData["id"] = "4";
            }
            //if audit session is win, id =4
            else if (HttpContext.Session.GetString("Audit") == "Win")
            {
                id = 5;
                TempData["id"] = "5";
            }


            var PYLContext = _context.audits.OrderByDescending(g => g.CreatedDate).Include(a => a.AuditType).ToList();
            //if id is null or 1 display all
            if (id==null)
            {
                PYLContext= _context.audits.OrderByDescending(g => g.CreatedDate).Include(a => a.AuditType).ToList();
            }
            else if(id==1)
            {
                PYLContext = _context.audits.OrderByDescending(g => g.CreatedDate).Include(a => a.AuditType).ToList();
            }
            //id 2 = cash in, so display only cash in type
            else if(id==2)
            {
                PYLContext= _context.audits.OrderByDescending(g => g.CreatedDate).Include(a => a.AuditType).Where(b => b.AuditTypeId =="A").ToList();
            }
            //id 3 = cash in, so display only cash our type
            else if (id==3)
            {
                PYLContext = _context.audits.OrderByDescending(g => g.CreatedDate).Include(a => a.AuditType).Where(b => b.AuditTypeId == "B").ToList();
            }
            //id 4 = cash in, so display only lose type
            else if (id == 4)
            {
                PYLContext = _context.audits.OrderByDescending(g => g.CreatedDate).Include(a => a.AuditType).Where(b => b.AuditTypeId == "D").ToList();
            }
            //id 5 = cash in, so display only win type
            else if (id == 5)
            {
                PYLContext = _context.audits.OrderByDescending(g => g.CreatedDate).Include(a => a.AuditType).Where(b => b.AuditTypeId == "C").ToList();
            }
            //if there is audit cash in sesstion
            if (HttpContext.Session.GetString("auditCashIn") != null)
            {
                //save cash in type to database after get the audit in session
                var audit = JsonConvert.DeserializeObject<Audit>(HttpContext.Session.GetString("auditCashIn"));
                _context.audits.Add(audit);
                _context.SaveChanges();
                //remove sesstion 
                HttpContext.Session.Remove("auditCashIn");
                return RedirectToAction("Index", "Home");
            }
            //if there is audit out in sesstion
            else if (HttpContext.Session.GetString("auditCashOut") != null)
            {
                //save cash in type to database after get the audit out session
                var audit = JsonConvert.DeserializeObject<Audit>(HttpContext.Session.GetString("auditCashOut"));
                _context.audits.Add(audit);
                _context.SaveChanges();
                HttpContext.Session.Remove("auditCashOut");
                return RedirectToAction("Index", "Player");
            }
            //if there is audit win in sesstion
            else if (HttpContext.Session.GetString("auditWin") != null)
            {
                //save cash in type to database after get the audit win session
                var audit = JsonConvert.DeserializeObject<Audit>(HttpContext.Session.GetString("auditWin"));
                _context.audits.Add(audit);
                _context.SaveChanges();
                HttpContext.Session.Remove("auditWin");
                return RedirectToAction("Index", "Home");
            }
            //if there is audit lose in sesstion
            else if (HttpContext.Session.GetString("auditLose") != null)
            {
                //save cash in type to database after get the audit lose session
                var audit = JsonConvert.DeserializeObject<Audit>(HttpContext.Session.GetString("auditLose"));
                _context.audits.Add(audit);
                _context.SaveChanges();
                HttpContext.Session.Remove("auditLose");
                return RedirectToAction("Index", "Home");
            }

            return View(PYLContext);
        }
    }
}
