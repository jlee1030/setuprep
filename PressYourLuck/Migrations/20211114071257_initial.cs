﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace PressYourLuck.Migrations
{
    public partial class initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "audits",
                columns: table => new
                {
                    AuditId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    PlayerName = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    CreatedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Amount = table.Column<double>(type: "float", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_audits", x => x.AuditId);
                });

            migrationBuilder.InsertData(
                table: "audits",
                columns: new[] { "AuditId", "Amount", "CreatedDate", "PlayerName" },
                values: new object[,]
                {
                    { 1, 10000.0, new DateTime(2021, 11, 10, 0, 0, 0, 0, DateTimeKind.Unspecified), "Jooho" },
                    { 2, 5000.0, new DateTime(2021, 11, 11, 0, 0, 0, 0, DateTimeKind.Unspecified), "Jooho" },
                    { 3, 1000.0, new DateTime(2021, 11, 12, 0, 0, 0, 0, DateTimeKind.Unspecified), "Jooho" },
                    { 4, 500.0, new DateTime(2021, 11, 13, 0, 0, 0, 0, DateTimeKind.Unspecified), "Jooho" }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "audits");
        }
    }
}
