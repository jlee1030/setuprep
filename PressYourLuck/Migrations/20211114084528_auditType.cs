﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace PressYourLuck.Migrations
{
    public partial class auditType : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "AuditTypeId",
                table: "audits",
                type: "nvarchar(450)",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "auditTypes",
                columns: table => new
                {
                    AuditTypeId = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_auditTypes", x => x.AuditTypeId);
                });

            migrationBuilder.InsertData(
                table: "auditTypes",
                columns: new[] { "AuditTypeId", "Name" },
                values: new object[,]
                {
                    { "A", "Cash In" },
                    { "B", "Cash Out" },
                    { "C", "Win" },
                    { "D", "Lose" }
                });

            migrationBuilder.UpdateData(
                table: "audits",
                keyColumn: "AuditId",
                keyValue: 1,
                column: "AuditTypeId",
                value: "A");

            migrationBuilder.UpdateData(
                table: "audits",
                keyColumn: "AuditId",
                keyValue: 2,
                column: "AuditTypeId",
                value: "B");

            migrationBuilder.UpdateData(
                table: "audits",
                keyColumn: "AuditId",
                keyValue: 3,
                column: "AuditTypeId",
                value: "C");

            migrationBuilder.UpdateData(
                table: "audits",
                keyColumn: "AuditId",
                keyValue: 4,
                column: "AuditTypeId",
                value: "D");

            migrationBuilder.CreateIndex(
                name: "IX_audits_AuditTypeId",
                table: "audits",
                column: "AuditTypeId");

            migrationBuilder.AddForeignKey(
                name: "FK_audits_auditTypes_AuditTypeId",
                table: "audits",
                column: "AuditTypeId",
                principalTable: "auditTypes",
                principalColumn: "AuditTypeId",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_audits_auditTypes_AuditTypeId",
                table: "audits");

            migrationBuilder.DropTable(
                name: "auditTypes");

            migrationBuilder.DropIndex(
                name: "IX_audits_AuditTypeId",
                table: "audits");

            migrationBuilder.DropColumn(
                name: "AuditTypeId",
                table: "audits");
        }
    }
}
