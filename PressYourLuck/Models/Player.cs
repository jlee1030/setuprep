﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace PressYourLuck.Models
{
    public class Player
    {
        [Required(ErrorMessage = "Please enter a name.")]
        public string Name { get; set; }

        [Range(1.00, 10000.00, ErrorMessage = "Number of coins should be between 1.00 and 10,000.00.")]
        public double TotalCoins { get; set; }
    }
}
