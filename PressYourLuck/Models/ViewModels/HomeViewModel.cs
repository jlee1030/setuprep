﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace PressYourLuck.Models.ViewModels
{
    public class HomeViewModel
    {
        public string PlayerName { get; set; }

        [Range(double.Epsilon, double.MaxValue, ErrorMessage = "bet must be greater than zero.")]
        public double bet { get; set; }
    }
}
