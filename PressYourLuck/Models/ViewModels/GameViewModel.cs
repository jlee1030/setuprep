﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PressYourLuck.Models.ViewModels
{
    public class GameViewModel
    {
        public List<Tile> TileList { get; set; }
        public double CurrentBet { get; set; }
    }
}
