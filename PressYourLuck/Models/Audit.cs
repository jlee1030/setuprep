﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace PressYourLuck.Models
{
    public class Audit
    {
        //validation and foreignkey
        public int AuditId { get; set; }
        [Required(ErrorMessage ="Please enter a player name")]
        public string PlayerName { get; set; }
        public DateTime CreatedDate { get; set; } = DateTime.Now;
        public double Amount { get; set; }

        [ForeignKey("AuditTypeId")]
        public string AuditTypeId { get; set; }
        public AuditType AuditType { get; set; }
    }
}
