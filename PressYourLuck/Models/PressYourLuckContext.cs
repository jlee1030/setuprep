﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PressYourLuck.Models
{
    public class PressYourLuckContext : DbContext
    {
        public PressYourLuckContext(DbContextOptions<PressYourLuckContext> options)
            : base(options)
        { }
        //db set for two models
        public DbSet<AuditType> auditTypes { get; set; }
        public DbSet<Audit> audits { get; set; }
        //data inital setting
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<AuditType>().HasData(
                new AuditType { AuditTypeId = "A", Name = "Cash In" },
                new AuditType { AuditTypeId = "B", Name = "Cash Out" },
                new AuditType { AuditTypeId = "C", Name = "Win" },
                new AuditType { AuditTypeId = "D", Name = "Lose" }
                );
            modelBuilder.Entity<Audit>().HasData(
                new Audit { AuditId = 1, PlayerName = "Jooho", CreatedDate = new DateTime(2021, 11, 10), Amount = 10000, AuditTypeId = "A" },
                new Audit { AuditId = 2, PlayerName = "Jooho", CreatedDate = new DateTime(2021, 11, 11), Amount = 5000, AuditTypeId = "B" },
                new Audit { AuditId = 3, PlayerName = "Jooho", CreatedDate = new DateTime(2021, 11, 12), Amount = 1000, AuditTypeId = "C" },
                new Audit { AuditId = 4, PlayerName = "Jooho", CreatedDate = new DateTime(2021, 11, 13), Amount = 500, AuditTypeId = "D" }
                );
        }
    }
}
