**Instructions**

1.Download visual stido and SSMS (SQL Server Management Studio) and open up this program.

2.In the program page, you can type "update-database" in the consol and click the "execute" button on the top.

3.When you run the program, you have to type your name and your buget how much you are going to load in the program.

4.After that, you should set your betting amount for one game.

5.When the game is started, you will see 10 cards.

6.You should click the "choose" button to card to reveal the value.

7.If it is busted, you lose your money, and if it there is a value, it will be multuply your betting amount.

8.You can stop the game if you win or continue for more money.

## License & copyright

© Jooho Lee, Conestoga College Computer Programming

Licensed under the [MIT License](LICENSE).

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

## Wiki

Click Wiki to see an article on the left side